function distance(first, second){
	
	if(!Array.isArray(first) || !Array.isArray(second))
	{
		throw new Error("InvalidType");
	}
	else if(first.length == 0 && second.length == 0)
	{
		return 0;
	}
	else
	{
		var d1 = d2 = 0;
		let cache = [];
		for(let x of first)
		{
			let common = false;
			let found = false;
			if(cache.length > 0) //check the cache
			{
				for(let c of cache)
				{
					if(c === x)
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					for(let y of second)
					{
						if(x === y)
						{
							common = true;
							break;
						}
					}
					if(!common)
					{
						d1++;
					}
				}
			}
			else //check in second
			{
				for(let y of second)
					{
						if(x === y)
						{
							common = true;
							break;
						}
					}
					if(!common)
					{
						d1++;
						
					}
			}
			cache.push(x);
			
		}
		
		cache = [];
		
		for(let x of second)
		{
			let common = false;
			let found = false;
			if(cache.length > 0) //check the cache
			{
				for(let c of cache)
				{
					if(c === x)
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					for(let y of first)
					{
						if(x === y)
						{
							common = true;
							break;
						}
					}
					if(!common)
					{
						d2++;
					}
				}
			}
			else //check in first
			{
				for(let y of first)
					{
						if(x === y)
						{
							common = true;
							break;
						}
					}
					if(!common)
					{
						d2++;
					}
			}
			cache.push(x);
			
		}
		
	}
	let d = d1 + d2;
	return d;
	
}


module.exports.distance = distance

let firstArr = ['a','a','a'];
let secondArr = ['a','c','d','c'];
let res = distance(firstArr,secondArr);
console.log(res);